<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DnaControllerTest
 * @package App\Tests\Controller
 */
final class DnaControllerTest extends WebTestCase
{
    public function testGetStatsWithoutToken(): void
    {
        $client = $this->createClient();
        $client->request('GET', '/stats');
        self::assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    public function testGetStats(): void
    {
        $client = $this->createClient();
        $client->request(
            'GET',
            '/stats',
            [],
            [],
            ['HTTP_AUTHORIZATION' => 'bearer '.JWT]
        );
        self::assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testWithMutation(): void
    {
        $client = $this->createClient();
        $client->request(
            'POST',
            '/mutation',
            [],
            [],
            ['HTTP_AUTHORIZATION' => 'bearer '.JWT],
            json_encode(['dna' => ['ATGCGA', 'CAGTGC', 'TTATGT', 'AGAAGG', 'CCCCTA', 'TCACTG']])
        );
        self::assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testTooSmallMatrix(): void
    {
        $client = $this->createClient();
        $client->request(
            'POST',
            '/mutation',
            [],
            [],
            ['HTTP_AUTHORIZATION' => 'bearer '.JWT],
            json_encode(['dna' => ['ATG', 'CAG', 'TTT']])
        );
        self::assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $client->getResponse()->getStatusCode());
    }

    public function testInvalidMatrix(): void
    {
        $client = $this->createClient();
        $client->request(
            'POST',
            '/mutation',
            [],
            [],
            ['HTTP_AUTHORIZATION' => 'bearer '.JWT],
            json_encode(['dna' => ['ATGCGA', 'CAGTGC', 'TTATGT', 'AGAAGG', 'CCCTA', 'TCACTG']])
        );
        self::assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $client->getResponse()->getStatusCode());
    }

    public function testInvalidChar(): void
    {
        $client = $this->createClient();
        $client->request(
            'POST',
            '/mutation',
            [],
            [],
            ['HTTP_AUTHORIZATION' => 'bearer '.JWT],
            json_encode(['dna' => ['ATGXGA', 'CAXTGC', 'TXATGT', 'AXAAGG', 'CCCCXA', 'XCACTG']])
        );
        self::assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $client->getResponse()->getStatusCode());
    }

    public function testRandomMutation(): void
    {
        $allowed = ['A', 'T', 'G', 'C'];
        $body = array_map(fn (string $string) => shuffle($allowed) ? implode('', $allowed) : '', $allowed);

        $client = $this->createClient();
        $client->request(
            'POST',
            '/mutation',
            [],
            [],
            ['HTTP_AUTHORIZATION' => 'bearer '.JWT],
            json_encode(['dna' => $body])
        );
        self::assertLessThan(Response::HTTP_INTERNAL_SERVER_ERROR, $client->getResponse()->getStatusCode());
    }

    public function testWithoutMutation(): void
    {
        $client = $this->createClient();
        $client->request(
            'POST',
            '/mutation',
            [],
            [],
            ['HTTP_AUTHORIZATION' => 'bearer '.JWT],
            json_encode(['dna' => ['ATGCGA', 'CAGTTC', 'TTATGT', 'AGGTGG', 'CTCCTA', 'TCACTG']])
        );
        self::assertEquals(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
    }
}
