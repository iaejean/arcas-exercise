# Arcas - DNA Excercise

[![pipeline status](https://gitlab.com/iaejean/arcas-exercise/badges/master/pipeline.svg)](https://gitlab.com/iaejean/arcas-exercise/-/commits/master)
[![coverage report](https://gitlab.com/iaejean/arcas-exercise/badges/master/coverage.svg)](https://gitlab.com/iaejean/arcas-exercise/-/commits/master)

## Requirements
- Docker

#### Running on local with Docker Compose
```shell script
$ docker network create dev-network
$ docker-compose up 
```

#### Access to docker image
```shell script
$ docker exec -it arcas_app_1 /bin/sh 
```

#### JWT can generate a new with the follow command
Then you will need to change it on phpunit.xml.dist file
```shell script
$  docker-compose exec app bin/console lexik:jwt:generate-token arcas
```

#### Run manually migrations and async process
````shell script
$ docker-compose exec app bin/console doctrine:migrations:migrate 
$ docker-compose exec app bin/console messenger:consume dna_analized --time-limit=3600 #supervisor already handle this task
````                
                   
#### Access to DB manually
````shell script
$ docker-compose exec db bash 
$ mysql -uroot -p
mysql> USE dna;
mysql> SHOW TABLES;
mysql> SELECT * FROM dna;
````               
#### API
you can access to API doc swagger int
```shell script
http://localhost:8080/api/doc
```                    
                                               
## Developers guide

### Docker - ECR
```bash
docker build . -t arcas_app && \
docker run -dit -p 8080:8080 -e APP_ENV="prod" --name arcas_app arcas_app
```

Attaching to the container
```bash
docker exec -it arcas_app /bin/sh
```

````shell script
$(aws ecr get-login --no-include-email --region us-east-2)
docker tag arcas_app:latest 746666869899.dkr.ecr.us-east-2.amazonaws.com/arcas:latest
docker push 746666869899.dkr.ecr.us-east-2.amazonaws.com/arcas:latest

````

### Gitlab Runner

```shell script
$ vim ~/.gitlab-runner/config.toml

privileged = true


$ gitlab-runner exec docker test --docker-privileged
$ gitlab-runner exec docker deploy --docker-privileged
```


```shell script
$ brew install gitlab-runner
$ brew services start gitlab-runner
$ gitlab-runner register
$ gitlab-runner exec docker test
```

### Contribution guidelines ###

Please consider read about some concepts like OOP, DDD, CQRS, Design Patterns, PSR1, PSR2, etc.
 
This project has been provided with several tools for ensure the code quality:

* PHP Code Sniffer
* PHP Mess Detector
* PHP CS Fixer
````shell script
$ docker-compose exec app vendor/bin/phpcbf --standard=PSR2 src/ tests/ 
$ docker-compose exec app vendor/bin/phpcbf --standard=PSR1 src/ tests/

$ docker-compose exec app vendor/bin/phpmd src/ xml codesize controversial design naming unusedcode --exclude=vendor/
$ docker-compose exec app vendor/bin/phpmd tests/ xml codesize controversial design naming unusedcode --exclude=vendor/

$ docker-compose exec app vendor/bin/php-cs-fixer fix src/ --dry-run --diff
````

* PHPMetrics
* PHPLoc

```shell script
$ docker-compose exec app vendor/bin/phploc src
$ docker-compose exec app vendor/bin/phpmetrics --report-html=report/metrics ./src --junit=junit.xml
```

Write unit test
* PHPUnit

````shell script
$ docker-compose exec app vendor/bin/simple-phpunit
````
                