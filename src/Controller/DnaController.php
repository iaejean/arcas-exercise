<?php

declare(strict_types=1);

namespace App\Controller;

use App\Dto\DnaStatsDto;
use App\Entity\Dna;
use App\Message\Query\AnalyzeMutability;
use App\Message\Query\GetStats;
use JMS\Serializer\SerializerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DnaController
 * @package App\Controller
 *
 * @Security(name="Bearer")
 * @SWG\Tag(name="Mutation")
 * @SWG\Response(response="500", description="An unexpected erros has ocurred")
 * @SWG\Response(response="401", description="Unauthorized request")
 */
final class DnaController extends AbstractController
{
    use HandleTrait;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * DnaController constructor.
     * @param MessageBusInterface $messageBus
     * @param SerializerInterface $serializer
     * @param LoggerInterface $logger
     */
    public function __construct(
        MessageBusInterface $messageBus,
        SerializerInterface $serializer,
        LoggerInterface $logger
    ) {
        $this->messageBus = $messageBus;
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    /**
     * @Route("/stats", methods={"GET"})
     * @SWG\Get(produces={"application/json"})
     * @SWG\Response(response="200", description="Stats", @Model(type=DnaStatsDto::class))
     *
     * @return Response
     */
    public function getStatsAction(): Response
    {
        $stats = $this->handle(new GetStats());

        return new Response($this->serializer->serialize($stats, 'json'), Response::HTTP_OK);
    }

    /**
     * @Route("/mutation", methods={"POST"})
     * @ParamConverter(name="dna", class="App\Entity\Dna")
     * @SWG\Post(consumes={"application/json"})
     * @SWG\Parameter(name="DnaRequest", in="body", @Model(type=Dna::class))
     * @SWG\Response(response="200", description="Request processed succesfully")
     * @SWG\Response(response="403", description="An unexpected erros has ocurred")
     *
     * @param Dna $dna
     * @return Response
     */
    public function hasMutationAction(Dna $dna): Response
    {
        $hasMutation = $this->handle(new AnalyzeMutability($dna));

        return new Response(null, $hasMutation ? Response::HTTP_OK : Response::HTTP_FORBIDDEN);
    }
}
