<?php

declare(strict_types=1);

namespace App\Message\Query;

use App\Entity\Dna;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AnalyzeMutability
 * @package App\Message\Query
 */
final class AnalyzeMutability
{
    /**
     * @Assert\Valid(groups={"default"})
     * @var Dna
     */
    private Dna $dna;

    /**
     * AnalyzeMutability constructor.
     * @param Dna $dna
     */
    public function __construct(Dna $dna)
    {
        $this->dna = $dna;
    }

    /**
     * @return Dna
     */
    public function getDna(): Dna
    {
        return $this->dna;
    }
}
