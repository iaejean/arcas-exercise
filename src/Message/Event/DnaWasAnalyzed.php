<?php

declare(strict_types=1);

namespace App\Message\Event;

use App\Entity\Dna;

/**
 * Class DnaWasAnalyzed
 * @package App\Message\Event
 */
final class DnaWasAnalyzed
{
    /**
     * @var Dna
     */
    private Dna $dna;

    /**
     * DnaWasAnalyzed constructor.
     * @param Dna $dna
     */
    public function __construct(Dna $dna)
    {
        $this->dna = $dna;
    }

    /**
     * @return Dna
     */
    public function getDna(): Dna
    {
        return $this->dna;
    }
}
