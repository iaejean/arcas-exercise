<?php

declare(strict_types=1);

namespace App\Repository;

use App\Contracts\Repository\DnaRepositoryInterface;
use App\Dto\DnaStatsDto;
use App\Entity\Dna;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class DnaRepository
 * @package App\Repository
 *
 * @method Dna|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dna|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dna[]    findAll()
 * @method Dna[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DnaRepository extends ServiceEntityRepository implements DnaRepositoryInterface
{
    /**
     * DnaRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dna::class);
    }

    /**
     * {@inheritdoc}
     */
    public function persist(Dna $dna): Dna
    {
        $this->getEntityManager()->persist($dna);
        $this->getEntityManager()->flush();

        return $dna;
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByHash(string $hash): ?Dna
    {
        return $this->findOneBy(['hash' => $hash]);
    }

    /**
     * {@inheritdoc}
     */
    public function getStats(): DnaStatsDto
    {
        $countWithMutation = 0;
        $countWithoutMutation = 0;

        $data = $this->createQueryBuilder('dna')
            ->select('COUNT(dna.uuid) AS total, IF(dna.hasMutation = TRUE, 1, 2) AS type')
            ->groupBy('dna.hasMutation')
            ->getQuery()
            ->getResult();

        foreach ($data as $col) {
            if (1 == $col['type']) {
                $countWithMutation = (float) $col['total'];
            } else {
                $countWithoutMutation = (float) $col['total'];
            }
        }

        return new DnaStatsDto($countWithMutation, $countWithoutMutation);
    }
}
