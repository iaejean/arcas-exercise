<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * Class Dna
 * @package App\Validator
 *
 * @Annotation
 */
final class Dna extends Constraint
{
    /**
     * @var string
     */
    public string $message = 'The value "{{ value }}" is not valid.';

    /**
     * @var string
     */
    public string $invalidCharsMessage = 'Only "{{ value }}" uppercase chars are allowed.';

    /**
     * @var string
     */
    public string $invalidYLengthMessage = 'The array should contains at least {{ value }} items.';

    /**
     * @var string
     */
    public string $invalidXLengthMessage = 'The string should contains {{ value }} length.';
}
