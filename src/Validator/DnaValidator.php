<?php

declare(strict_types=1);

namespace App\Validator;

use App\Contracts\Services\DnaServiceInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class DnaValidator
 * @package App\Validator
 */
final class DnaValidator extends ConstraintValidator
{
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * DnaValidator constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param mixed|array $value
     * @param Constraint&Dna $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (null === $value || '' === $value) {
            return;
        }

        $yLength = count($value);
        if ($yLength < DnaServiceInterface::LENGTH) {
            $this->context->buildViolation($constraint->invalidYLengthMessage)
                ->setParameter('{{ value }}', (string) DnaServiceInterface::LENGTH)
                ->addViolation();
        }

        $data = array_map(fn (string $chain) => strlen($chain) === $yLength, $value);
        if (in_array(false, $data)) {
            $this->context->buildViolation($constraint->invalidXLengthMessage)
                ->setParameter('{{ value }}', (string) $yLength)
                ->addViolation();
        }

        $data = array_map(
            fn (string $chain) => empty(preg_match('/[^'.implode(DnaServiceInterface::BASE).']/', $chain)),
            $value
        );
        if (in_array(false, $data)) {
            $this->context->buildViolation($constraint->invalidCharsMessage)
                ->setParameter('{{ value }}', implode(DnaServiceInterface::BASE))
                ->addViolation();
        }
    }
}
