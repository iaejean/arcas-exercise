<?php

declare(strict_types=1);

namespace App\Contracts\Repository;

use App\Dto\DnaStatsDto;
use App\Entity\Dna;

/**
 * Interface DnaRepositoryInterface
 * @package App\Contracts\Repository
 */
interface DnaRepositoryInterface
{
    /**
     * @param Dna $dna
     * @return Dna
     */
    public function persist(Dna $dna): Dna;

    /**
     * @return DnaStatsDto
     */
    public function getStats(): DnaStatsDto;

    /**
     * @param string $hash
     * @return Dna|null
     */
    public function findOneByHash(string $hash): ?Dna;
}
