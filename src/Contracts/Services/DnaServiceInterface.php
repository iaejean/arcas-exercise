<?php

declare(strict_types=1);

namespace App\Contracts\Services;

use App\Dto\DnaStatsDto;
use App\Entity\Dna;

/**
 * Interface DnaServiceInterface
 * @package App\Contracts\Services
 */
interface DnaServiceInterface
{
    public const BASE = ['A', 'T', 'C', 'G'];
    public const LENGTH = 4;
    public const SEQUENCE = 2;

    /**
     * @param Dna $dna
     * @return Dna
     */
    public function add(Dna $dna): Dna;

    /**
     * @param Dna $dna
     * @return bool
     */
    public function hasMutation(Dna $dna): bool;

    /**
     * @return DnaStatsDto
     */
    public function getStats(): DnaStatsDto;
}
