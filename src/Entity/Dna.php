<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\DnaRepository;
use App\Validator as Validator;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class Dna
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass=DnaRepository::class)
 * @ORM\Table(name="dna")
 * @UniqueEntity(fields={"dna"})
 */
class Dna
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="string")
     * @Serializer\Exclude()
     *
     * @var string
     */
    private string $uuid;

    /**
     * @ORM\Column(type="json", length=255)
     * @Serializer\Type("array<string>")
     * @Validator\Dna(groups={"default"})
     *
     * @var array
     */
    private array $dna;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Exclude()
     *
     * @var string
     */
    private string $hash;

    /**
     * @ORM\Column(type="boolean")
     * @Serializer\Exclude()
     *
     * @var bool
     */
    private bool $hasMutation;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Serializer\Exclude()
     *
     * @var \DateTimeInterface
     */
    private \DateTimeInterface $createdAt;

    /**
     * @Serializer\PostDeserialize()
     */
    public function preSerialize(): void
    {
        $this->createdAt = new \DateTime();
        $this->setHash();
    }

    /**
     * @param bool $hasMutation
     * @return Dna
     */
    public function setHasMutation(bool $hasMutation): Dna
    {
        $this->hasMutation = $hasMutation;

        return $this;
    }

    /**
     * @return Dna
     */
    public function setHash(): Dna
    {
        $this->hash = hash('sha1', json_encode($this->dna));

        return $this;
    }

    /**
     * @return array
     */
    public function getDna(): array
    {
        return $this->dna;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }
}
