<?php

declare(strict_types=1);

namespace App\Dto;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class DnaStatsDto
 * @package App\Dto
 */
final class DnaStatsDto
{
    /**
     * @Serializer\Type("float")
     * @var float
     */
    private float $countMutations;

    /**
     * @Serializer\Type("float")
     * @var float
     */
    private float $countNoMutation;

    /**
     * DnaStatsDto constructor.
     * @param float $countMutations
     * @param float $countNoMutation
     */
    public function __construct(float $countMutations, float $countNoMutation)
    {
        $this->countMutations = $countMutations;
        $this->countNoMutation = $countNoMutation;
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\Expose()
     * @return float
     */
    public function getRatio(): float
    {
        return $this->countMutations / $this->countNoMutation;
    }
}
