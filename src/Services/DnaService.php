<?php

declare(strict_types=1);

namespace App\Services;

use App\Contracts\Repository\DnaRepositoryInterface;
use App\Contracts\Services\DnaServiceInterface;
use App\Dto\DnaStatsDto;
use App\Entity\Dna;
use App\Message\Event\DnaWasAnalyzed;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class DnaService
 * @package App\Services
 */
final class DnaService implements DnaServiceInterface
{
    /**
     * @var DnaRepositoryInterface
     */
    private DnaRepositoryInterface $dnaRepository;

    /**
     * @var MessageBusInterface
     */
    private MessageBusInterface $messageBus;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * DnaService constructor.
     * @param DnaRepositoryInterface $dnaRepository
     * @param MessageBusInterface $messageBus
     * @param LoggerInterface $logger
     */
    public function __construct(
        DnaRepositoryInterface $dnaRepository,
        MessageBusInterface $messageBus,
        LoggerInterface $logger
    ) {
        $this->dnaRepository = $dnaRepository;
        $this->messageBus = $messageBus;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function add(Dna $dna): Dna
    {
        if (!$this->exist($dna)) {
            return $this->dnaRepository->persist($dna);
        }

        return $dna;
    }

    /**
     * {@inheritdoc}
     */
    private function exist(Dna $dna): bool
    {
        $dna = $this->dnaRepository->findOneByHash($dna->getHash());

        return $dna instanceof Dna;
    }

    /**
     * {@inheritdoc}
     */
    public function hasMutation(Dna $dna): bool
    {
        $sequenceFound = 0;
        $required = DnaServiceInterface::SEQUENCE;
        $sequenceFound = $this->searchInHorizontal($required, $dna->getDna(), $sequenceFound);
        if (DnaServiceInterface::SEQUENCE <= $sequenceFound) {
            return $this->dispatchDnaAnalysisResult($dna, true);
        }

        $sequenceFound = $this->searchInVertical($required, $dna->getDna(), $sequenceFound);
        if (DnaServiceInterface::SEQUENCE <= $sequenceFound) {
            return $this->dispatchDnaAnalysisResult($dna, true);
        }

        $sequenceFound = $this->searchInDiagonal($required, $dna->getDna(), $sequenceFound);
        if (DnaServiceInterface::SEQUENCE <= $sequenceFound) {
            return $this->dispatchDnaAnalysisResult($dna, true);
        }

        return $this->dispatchDnaAnalysisResult($dna, false);
    }

    /**
     * @param Dna $dna
     * @param bool $hasMutation
     * @return bool
     */
    private function dispatchDnaAnalysisResult(Dna $dna, bool $hasMutation): bool
    {
        $dna->setHasMutation($hasMutation);
        $this->messageBus->dispatch(new DnaWasAnalyzed($dna));

        return $hasMutation;
    }

    /**
     * @param int $required
     * @param array $array
     * @param int $startsWith
     * @return int
     */
    private function findSequence(
        int $required = DnaServiceInterface::LENGTH,
        array $array = [],
        $startsWith = 0
    ): int {
        $foundSequence = $startsWith;
        foreach (DnaServiceInterface::BASE as $char) {
            if (!empty(preg_grep(sprintf('/(%s{%s})/', $char, (string) DnaServiceInterface::LENGTH), $array))) {
                $this->logger->info(sprintf('Find sequence "%s"', $char));
                ++$foundSequence;
                if ($required === $foundSequence) {
                    return $foundSequence;
                }
            }
        }

        return $foundSequence;
    }

    /**
     * @param int $required
     * @param array $array
     * @param int $startsWith
     * @return int
     */
    private function searchInHorizontal(
        int $required = DnaServiceInterface::LENGTH,
        array $array = [],
        $startsWith = 0
    ): int {
        return $this->findSequence($required, $array, $startsWith);
    }

    /**
     * @param int $required
     * @param array $array
     * @param int $startsWith
     * @return int
     */
    private function searchInVertical(
        int $required = DnaServiceInterface::LENGTH,
        array $array = [],
        $startsWith = 0
    ): int {
        $length = count($array);
        $matrix = [];

        for ($i = 0; $i < $length; ++$i) {
            $newString = '';
            for ($j = 0; $j < $length; ++$j) {
                $newString .= $array[$j][$i];
            }
            $matrix[] = $newString;
        }

        return $this->findSequence($required, $matrix, $startsWith);
    }

    /**
     * @param int $required
     * @param array $array
     * @param int $startsWith
     * @return int
     */
    private function searchInDiagonal(
        int $required = DnaServiceInterface::LENGTH,
        array $array = [],
        $startsWith = 0
    ): int {
        $length = count($array);
        $newMatrixDiagonalLength = ($length * 2) - 1;
        $newMatrix = [];

        for ($i = 0; $i < $newMatrixDiagonalLength; ++$i) {
            $newString = '';
            $newStringInverse = '';
            for ($j = min([$i, $length - 1]); $j > max([-1, $i - $length]); --$j) {
                $newString .= $array[$j][$i - $j];
                $newStringInverse .= $array[$length - 1 - $j][$i - $j];
            }
            $newMatrix[] = $newStringInverse;
            $newMatrix[] = $newString;
        }

        return $this->findSequence($required, $newMatrix, $startsWith);
    }

    /**
     * {@inheritdoc}
     */
    public function getStats(): DnaStatsDto
    {
        return $this->dnaRepository->getStats();
    }
}
