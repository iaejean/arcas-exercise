<?php

declare(strict_types=1);

namespace App\MessageHandler\Event;

use App\Contracts\Services\DnaServiceInterface;
use App\Message\Event\DnaWasAnalyzed;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Class DnaWasAnalyzedHandler
 * @package App\MessageHandler\Event
 */
final class DnaWasAnalyzedHandler implements MessageHandlerInterface
{
    /**
     * @var DnaServiceInterface
     */
    private DnaServiceInterface $dnaService;

    /**
     * DnaWasAnalyzedHandler constructor.
     * @param DnaServiceInterface $dnaService
     */
    public function __construct(DnaServiceInterface $dnaService)
    {
        $this->dnaService = $dnaService;
    }

    /**
     * @param DnaWasAnalyzed $message
     */
    public function __invoke(DnaWasAnalyzed $message)
    {
        $this->dnaService->add($message->getDna());
    }
}
