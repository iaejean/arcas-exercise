<?php

declare(strict_types=1);

namespace App\MessageHandler\Query;

use App\Contracts\Services\DnaServiceInterface;
use App\Message\Query\AnalyzeMutability;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class AnalyzeMutabilityHandler
 * @package App\MessageHandler\Query
 */
final class AnalyzeMutabilityHandler implements MessageHandlerInterface
{
    /**
     * @var DnaServiceInterface
     */
    private DnaServiceInterface $dnaService;

    /**
     * @var MessageBusInterface
     */
    private MessageBusInterface $messageBus;

    /**
     * AnalyzeMutabilityHandler constructor.
     * @param DnaServiceInterface $dnaService
     * @param MessageBusInterface $messageBus
     */
    public function __construct(DnaServiceInterface $dnaService, MessageBusInterface $messageBus)
    {
        $this->dnaService = $dnaService;
        $this->messageBus = $messageBus;
    }

    /**
     * @param AnalyzeMutability $message
     * @return bool
     */
    public function __invoke(AnalyzeMutability $message): bool
    {
        return $this->dnaService->hasMutation($message->getDna());
    }
}
