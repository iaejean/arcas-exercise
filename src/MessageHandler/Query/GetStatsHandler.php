<?php

declare(strict_types=1);

namespace App\MessageHandler\Query;

use App\Dto\DnaStatsDto;
use App\Message\Query\GetStats;
use App\Services\DnaService;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Class GetStatsHandler
 * @package App\MessageHandler\Query
 */
final class GetStatsHandler implements MessageHandlerInterface
{
    /**
     * @var DnaService
     */
    private DnaService $dnaService;

    /**
     * GetStatsHandler constructor.
     * @param DnaService $dnaService
     */
    public function __construct(DnaService $dnaService)
    {
        $this->dnaService = $dnaService;
    }

    /**
     * @param GetStats $message
     * @return DnaStatsDto
     */
    public function __invoke(GetStats $message)
    {
        return $this->dnaService->getStats();
    }
}
