<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20200509024026
 * @package DoctrineMigrations
 */
final class Version20200509024026 extends AbstractMigration
{
    /**
     * {@inheritdoc}
     */
    public function getDescription(): string
    {
        return 'Create table DNA';
    }

    /**
     * {@inheritdoc}
     */
    public function up(Schema $schema): void
    {
        $this->addSql('
            CREATE TABLE dna(
                uuid VARCHAR(45) NOT NULL PRIMARY KEY,
                dna JSON NOT NULL,
                has_mutation BOOL NOT NULL,
                hash VARCHAR(70) NOT NULL,
                created_at DATETIME NOT NULL DEFAULT current_timestamp(),
                KEY hash_idx (hash)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            INSERT INTO dna (uuid, dna, has_mutation, hash, created_at)
            VALUES
                ("8c0969ce-9353-11ea-9107-3611b24956d2", \'["GCAT", "TGAC", "ACTG", "GATC"]\', 0, "131c750274a7a8906d860a512e8fc57ecb9c0074", NOW()),
                ("8c0b360a-9353-11ea-9107-3611b24956d2", \'["ATGCGA", "CAGTTC", "TTATGT", "AGGTGG", "CTCCTA", "TCACTG"]\', 0, "82d9b4013d575c10d975a7749dcd72fb728dc521", NOW()),
                ("cd9c486c-9352-11ea-9107-3611b24956d2", \'["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]\', 1, "b05c55f84d9a3028390e2bca8a3d8d2497e14992", NOW());
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function down(Schema $schema): void
    {
        $this->addSql('
            DROP table dna;
        ');
    }
}
