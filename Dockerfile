FROM php:7.4.5-fpm-alpine

MAINTAINER Israel Hernández <iaejeanx@gmail.com>

COPY . /app
WORKDIR /app

RUN apk add --no-cache tzdata
ENV TZ=America/Mexico_City
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apk add --update --no-cache --virtual .build-dependencies $PHPIZE_DEPS \
    nginx \
    coreutils \
    libmcrypt-dev \
    libxml2-dev \
    libpng \
    libpng-dev \
    freetype \
    freetype-dev \
    libjpeg-turbo \
    libjpeg-turbo-dev \
    libxslt-dev \
    libzip-dev \
    libgcrypt-dev \
    gmp-dev \
    openssh \
    icu-dev \
    supervisor \
    autoconf \
    && docker-php-ext-configure gd \
        --enable-gd \
        --with-freetype=/usr/include/ \
        --with-jpeg=/usr/include/ \
    && docker-php-ext-install \
    soap \
    gd \
    gmp \
    calendar \
    bcmath \
    opcache \
    xsl \
    zip \
    intl \
    pdo_mysql \
    && pecl install apcu \
    && docker-php-ext-enable apcu

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
RUN composer global require hirak/prestissimo
RUN composer install --optimize-autoloader --no-interaction --no-progress

RUN mkdir -p /run/nginx \
    && mkdir /etc/nginx/sites-enabled/ \
    && rm -Rf /etc/nginx/conf.d/default.conf \
    && touch /var/log/nginx/access.log \
    && touch /var/log/nginx/error.log \
    && chown nginx /var/log/nginx/*log \
    && chmod 644 /var/log/nginx/*log

COPY docker/php /usr/local/etc/php/conf.d
COPY docker/nginx/nginx.conf /etc/nginx/
COPY docker/nginx/sites-enabled/ /etc/nginx/sites-enabled
COPY docker/supervisor/ /etc/supervisor

EXPOSE 8080

ENTRYPOINT sh ./docker/app/start.sh
