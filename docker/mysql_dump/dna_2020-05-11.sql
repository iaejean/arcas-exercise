# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.26)
# Database: dna
# Generation Time: 2020-05-11 07:13:46 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table dna
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dna`;

CREATE TABLE `dna` (
  `uuid` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dna` json NOT NULL,
  `has_mutation` tinyint(1) NOT NULL,
  `hash` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid`),
  KEY `hash_idx` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `dna` WRITE;
/*!40000 ALTER TABLE `dna` DISABLE KEYS */;

INSERT INTO `dna` (`uuid`, `dna`, `has_mutation`, `hash`, `created_at`)
VALUES
	('8c0969ce-9353-11ea-9107-3611b24956d2',X'5B2247434154222C202254474143222C202241435447222C202247415443225D',0,'131c750274a7a8906d860a512e8fc57ecb9c0074','2020-05-11 01:48:31'),
	('8c0b360a-9353-11ea-9107-3611b24956d2',X'5B22415447434741222C2022434147545443222C2022545441544754222C2022414747544747222C2022435443435441222C2022544341435447225D',0,'82d9b4013d575c10d975a7749dcd72fb728dc521','2020-05-11 01:48:31'),
	('cd9c486c-9352-11ea-9107-3611b24956d2',X'5B22415447434741222C2022434147544743222C2022545441544754222C2022414741414747222C2022434343435441222C2022544341435447225D',1,'b05c55f84d9a3028390e2bca8a3d8d2497e14992','2020-05-11 01:43:50');

/*!40000 ALTER TABLE `dna` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table messenger_message
# ------------------------------------------------------------

DROP TABLE IF EXISTS `messenger_message`;

CREATE TABLE `messenger_message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `headers` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `available_at` datetime NOT NULL,
  `delivered_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8F9F0C97FB7336F0` (`queue_name`),
  KEY `IDX_8F9F0C97E3BD61CE` (`available_at`),
  KEY `IDX_8F9F0C9716BA31DB` (`delivered_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table migration_versions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migration_versions`;

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;

INSERT INTO `migration_versions` (`version`, `executed_at`)
VALUES
	('20200509024026','2020-05-10 01:15:45');

/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
