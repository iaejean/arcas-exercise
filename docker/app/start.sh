#!/bin/bash

echo '['$(date +"%Y-%m-%d %H:%M:%S")'] Starting nginx'
nginx -g "daemon on;"

echo '['$(date +"%Y-%m-%d %H:%M:%S")'] Granting permissions for var directory'
rm -Rf var

rm -Rf .git

echo '['$(date +"%Y-%m-%d %H:%M:%S")'] Clearing Symfony cache'
php bin/console cache:clear

echo '['$(date +"%Y-%m-%d %H:%M:%S")'] Granting permissions for var directory'
chmod -R 777 var

echo '['$(date +"%Y-%m-%d %H:%M:%S")'] Starting supervisord'
supervisord -c /etc/supervisor/supervisord.conf &

echo '['$(date +"%Y-%m-%d %H:%M:%S")'] Running php-fpm'
php-fpm -F
