#!/bin/bash

echo '['$(date +"%Y-%m-%d %H:%M:%S")'] Starting supervisord'
supervisord -c /etc/supervisor/supervisord.conf &

echo '['$(date +"%Y-%m-%d %H:%M:%S")'] Running php-fpm'
php-fpm -F
